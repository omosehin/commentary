
    export const validationRules = () =>{
        const emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        return {        
          secret: val =>
            val.length < 6 ? `Password length  must be longer than 5!` : "",
          userEmail: val =>
            !emailRegex.test(val) ? "Valid email address is required!" : ""
        };
    }