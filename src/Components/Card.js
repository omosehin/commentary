import React from 'react'
import {Link} from 'react-router-dom'
import '../App.css'

 const Card = ({title,className,classNameIcon, cardtext,state,amount,type,src,alt,imgClassName,login,More}) => {
    return (
        <div className = {className}>
            <div>
            <img src = {src} alt = {alt} className = {imgClassName}/>
                <h2>{login}</h2>
                <Link to= {`/User/${login}`} style ={{fontSize:'1.5rem'}}>{More}</Link>
            </div>
            <i className={classNameIcon}></i>
            <h2 className = 'card-title'>{title}</h2>  
            <p className = 'card-text'>{cardtext}</p> 
            <div >
                <ul className= "jobList">
                    <li className ='stateAmount'>
                    {state}
                    </li>
                    <li className ='stateAmount'>
                    {amount}
                    </li>
                    <li className ='stateAmount'>
                    {type}
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default Card;