import React, { useState, useEffect } from "react";
import axios from "axios";
import "./../App.css";
import Nav from "../Components/Nav";
import Card from "../Components/Card";
import Spinner from "./Spinner/Spinner";
import Alert from "./Alert";
import SearchBlog from "./SearchBlog";


const Blog = () => {
  const [users, setUser] = useState([]);
  const [loading, setLoading] = useState(false);
  const [alerts, setalert] = useState(null);

  useEffect(() => {
    setLoading(true);
    async function fetchUser() {
      const res = await axios.get(
        `https://api.github.com/users?client_id=${process.env.REACT_APP_GITHUB_CLIENT_ID}&client_secret=${process.env.REACT_APP_GITHUB_CLIENT_SECRET}`
      );
      setLoading(false);
      setUser(res.data);
    }
    fetchUser();
  }, []);

  const searchUsers = searchText => {
    setLoading(true);
    async function searchUser() {
      const res = await axios.get(
        `https://api.github.com/search/users?q=${searchText}&client_id=${process.env.REACT_APP_GITHUB_CLIENT_ID}&client_secret=${process.env.REACT_APP_GITHUB_CLIENT_SECRET}`
      );
      setLoading(false);
      setUser(res.data.items);
    }
    searchUser();
  };

  const setAlert = (msg, alert) => {
    setalert({ msg, alert });
    setTimeout(() => {
      setAlert("");
    }, 5000);
  };

  return (
    <div>
      <Nav />
      <Alert myalert={alerts} />
      <SearchBlog searchUsers={searchUsers} users={users} setAlert={setAlert} />
      {loading === true ? (
        <Spinner />
      ) : (
        <div className="container">
          <div className="rowblog">
            {users &&
              users.map(user => (
                <div key={user.id} className="userCard">
                  <Card
                    src={user.avatar_url}
                    login={user.login}
                    imgClassName="imgClassName"
                    More = 'More'
                  />
                </div>
              ))}
          </div>
        </div>
      )}
    </div>
  );
};

export default Blog;
