import React from "react";
import Proptypes from "prop-types";

const Repos = ({ repos }) => (
  <div className="container">
    <h3 className="ml-5">5 Recent Repo</h3>
    <div className="row">
      {repos.map(repo => (
        <div key={repo.id} className="col-lg-10 card mx-auto py-4 mt-4">
          <a href={repo.html_url} className="repoName">
            {repo.name}
          </a>
        </div>
      ))}
      ;
    </div>
  </div>
);

Repos.protoTypes = {
  repos: Proptypes.array.isRequired
};
export default Repos;
