import React, { Fragment, useState, useEffect } from "react";
import { Link,
  useParams
} from "react-router-dom";
import axios from "axios";
import Spinner from "./Spinner/Spinner";
import "../App.css";
import Nav from "./Nav";
import Repos from "./Repos/Repos";

function User() {
  const [user, setUser] = useState([]);
  const [loading, setLoading] = useState(false);
  const [repos, setRepos] = useState([]);
  let { login } = useParams();

  useEffect(() => {
    setLoading(true);
    async function fetchUser() {
      const res = await axios.get(
        `https://api.github.com/users/${login}?client_id=${process.env.REACT_APP_GITHUB_CLIENT_ID}&client_secret=${process.env.REACT_APP_GITHUB_CLIENT_SECRET}`
      );
      setLoading(false);
      setUser(res.data);
    }
    fetchUser();
  }, [login]);

  useEffect(() => {
    setLoading(true);
    const fetchUserRepo = async () => {
      const res = await axios.get(
        `https://api.github.com/users/${login}/repos?per_page=5&sort=created:asc&client_id=${process.env.REACT_APP_GITHUB_CLIENT_ID}&client_secret=${process.env.REACT_APP_GITHUB_CLIENT_SECRET}`
      );
      setLoading(false);
      setRepos(res.data);
    };
    fetchUserRepo();
  }, [login]);

  const {
    name,
    avatar_url,
    location,
    bio,
    company,
    blog,
    followers,
    following,
    public_repos,
    public_gists,
    hireable
  } = user;

  if (loading) return <Spinner />;
  return (
    <Fragment>
      <Nav />

      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <Link to="/Blog" className="badgesText">
              Search User
            </Link>
            <span className="ml-4">
              Hireable:{" "}
              {hireable ? (
                <i className="fas fa-check text-success"></i>
              ) : (
                <i className="fas fa-times-circle text-danger"></i>
              )}
            </span>
          </div>
        </div>
      </div>

      <div className="container card pt-3">
        <div className=" row align-items-center">
          <div className="col-md-4">
            <img src={avatar_url} alt="user" className="round-img" />
            <h1>{login}</h1>
            <h5 style ={{color:'green'}}>{location}</h5>
          </div>

          <div className="col-md-8 userCardBio">
            {bio && (
              <Fragment>
                <h2>Bio</h2>
                <div className="userbio">{bio}</div>
              </Fragment>
            )}
            <h1 className="btn btn-dark">Visit Github Profile</h1>
            <ul className="userListText">
              <li>
                {login && (
                  <Fragment>
                    <strong className="letter-spacing">Name: </strong>
                    <span className="letter-spacing"> {name}</span>
                  </Fragment>
                )}
              </li>
              <li>
                {login && (
                  <Fragment>
                    <strong className="letter-spacing">Username: </strong>

                    <span className="letter-spacing"> {login}</span>
                  </Fragment>
                )}
              </li>
              <li>
                {company && (
                  <Fragment>
                    <strong className="letter-spacing">Company: </strong>
                    <span className="letter-spacing">{company}</span>
                  </Fragment>
                )}
              </li>
              <li>
                {blog && (
                  <Fragment>
                    <strong className="letter-spacing">Website: </strong>
                    <span className="letter-spacing"> {blog}</span>
                  </Fragment>
                )}
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="container" style={{ textAlign: "center" }}>
        <div className="row">
          <div className="col-md-3">
            <div className="badge badge-primary py-4 badgesText">
              Followers:{followers}
            </div>
          </div>
          <div className="col-md-3">
            <div className="badge badge-success py-4 badgesText">
              Following:{following}
            </div>
          </div>
          <div className="col-md-3">
            <div className="badge badge-light py-4 badgesText">
              Public Repos:{public_repos}
            </div>
          </div>
          <div className="col-md-3">
            <div className="badge badge-dark py-4 badgesText">
              Public Gists:{public_gists}
            </div>
          </div>
        </div>
      </div>
      <Repos repos = {repos}/>
    </Fragment>
  );
}
export default User;
