import React, { useState, useEffect } from "react";
import Nav from "./Nav";
// import SearchForm from './Landingpage/SearchForm';

const headerText = ["GET EMPLOYED", "GET FIXED UP", " GET ENGAGED"];
const colorText = ["green", "purple", "brown"];

// const randomColor =  headerText[Math.floor(Math.random() * headerText.length)];

const Header = prop => {
  const [current, setCurrent] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      if (current === headerText.length && current === colorText.length) {
        setCurrent(0);
      } else {
        setCurrent(current + 1);
      }
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  });

  return (
    <div>
      <header className={prop.myClass}>
        <Nav />
        <div className="header_content">
          <h1 className={`MainHeader ${colorText[current]}`}>
            {headerText[current]}
          </h1>
        </div>
        <div className='headerText'>

          Get Your Dream Job With Us
        </div>
        {/* <SearchForm/> */}
      </header>
    </div>
  );
};

export default Header;
