import React from 'react';
import '../../App.css';
import Header from '../Header';
import Card from '../Card';
import Footer from '../Footer';


const Home = () => {
  
  return (
    <div className="">
      <Header myClass="topSection" />
      <div className="products">
        <h1 className = 'heading' style ={{marginTop:'50px'}}>THE RIGHT JOB IS HERE</h1>
        <p className="productParagraph">Fugiat excepteur veniam ad id aliqua esse voluptate fugiat tempor cupidatat consequat. Reprehenderit incididunt quis adipisicing ex do voluptate. Ex do anim in ad fugiat culpa dolore minim eiusmod.
                Elit eiusmod fugiat mollit velit duis amet laborum minim ut minim amet in cupidatat. .</p>
      </div>
      <div className="SectionA">
          <Card className="card_1" classNameIcon='fa fa-search-plus fa-5x icons' title={'Find the Right Team'} cardtext = {'excepteur veniam ad id aliqua esse tempor cupidatat consequat veniam ad id aliqua esse minim ut minim ame '} />
        
          <Card className="card_2" classNameIcon='fa fa-user-circle fa-5x icons' title={'Find the Right Job'}  cardtext = {'excepteur veniam ad id aliqua esse tempor cupidatat consequat veniam ad id aliqua esse minim ut minim ame '}/>
       
          <Card className="card_3" classNameIcon='fa fa-magnet fa-5x icons' title={'Get the right Job'}  cardtext = {'excepteur veniam ad id aliqua esse tempor cupidatat consequat veniam ad id aliqua esse minim ut minim ame '} />
      </div>


      <div className="SectionB">
      <h1 className = 'heading' style ={{textAlign:'center',color:'white'}}>CURRENT JOB OPENINGS</h1>
      <div className="cards">
     
          <Card className="card_job" title={'Software Engineer'} cardtext = {'excepteur veniam ad id aliqua esse tempor cupidatat consequat veniam ad id aliqua esse minim ut minim ame '} state='Lagos' amount='500,000' type='full-time' />
        
          <Card className="card_job"  title={'Lawyer'}  cardtext = {'excepteur veniam ad id aliqua esse tempor cupidatat consequat veniam ad id aliqua esse minim ut minim ame '}  state='Lagos' amount='500,000' type='full-time' />
       
          <Card className="card_job"  title={'Architect'}  cardtext = {'excepteur veniam ad id aliqua esse tempor cupidatat consequat veniam ad id aliqua esse minim ut minim ame '}  state='Lagos' amount='500,000' type='full-time' />
          <Card className="card_job" title={'Software Engineer'} cardtext = {'excepteur veniam ad id aliqua esse tempor cupidatat consequat veniam ad id aliqua esse minim ut minim ame '}  state='Lagos' amount='500,000' type='full-time' />
        
          <Card className="card_job"  title={'Lawyer'}  cardtext = {'excepteur veniam ad id aliqua esse tempor cupidatat consequat veniam ad id aliqua esse minim ut minim ame '} state='Lagos' amount='500,000' type='full-time' />
       
          <Card className="card_job"  title={'Architect'}  cardtext = {'excepteur veniam ad id aliqua esse tempor cupidatat consequat veniam ad id aliqua esse minim ut minim ame '}  state='Lagos' amount='500,000' type='full-time' />
      </div>
      </div>
      
      <footer>
        <Footer />
      </footer>
    </div>

    
  );
}

export default Home;
