
import React, { useState } from 'react';
import Button from '../Button';
import '../../App.css'

const initialState = {
    location :'',
    title : '',
    type :''
}

const SearchForm = () => {
    const [search, setsearch] = useState(initialState);

    const handleChange = (e) =>{
        const {name, value} = e.target;
        setsearch({...search,[name]:value})
    }

    const  handleSubmit = (e)=>{
        e.preventDefault()
            setsearch(initialState);
            console.log(search);
    }
    const {type,location,title} = search
    return (
        <div>
            <h2 className = 'Get-Job-Heading'>GET THE RIGHT JOB OF YOUR CHOICE</h2>
        <form className='SearchForm' onSubmit = {handleSubmit} >
            <select name="location" id='location' value= {location} onChange= {handleChange}>
                <option value="">Please choose Location</option>
                <option value="Lagos">Lagos</option>
                <option value="Akure">Akure</option>
                <option value="Abuja">Abuja</option>
                <option value="Owerri">Owerri</option>

            </select>
        

            <select name="type" id='type' value = {type} onChange= {handleChange}>
                <option value="">Please choose Job Type</option>
                <option value="FullTime">Full Time</option>
                <option value="intern">Intern</option>
                <option value="contract">Contract</option>
                <option value="Freelance">Freelance</option>

            </select>

    
            <select name="title" id='location' value = {title} onChange= {handleChange}>
                <option value="">Please choose Job Title</option>
                <option value="Manager">FManager</option>
                <option value="Driver">Driver</option>
            </select>
           
<Button type='submit' onClick = {handleSubmit} disabled= {!type || !location || !title} className = {!type || !location || !title ?'disabled' :''}>
    SEARCH
</Button>
        </form>
        </div>
    );
};
export default SearchForm