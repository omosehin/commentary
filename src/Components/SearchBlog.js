import React, { useState } from "react";
import Button from "./Button";
import './../App.css'

 function SearchBlog({users,searchUsers,setAlert}) {
    const [search, setsearch] = useState('');

    const onSeachChange =(e) =>{
        setsearch(e.target.value)
        }
        
        const handleSubmit =e=>{
            e.preventDefault();
            if(search ===''){
                setAlert('Please Enter Field','light')
            }
            else{
                searchUsers(search);
                setsearch('')
            }
            
         }
    
    return (
        <div>
            {users && <div>
        <form onSubmit = {handleSubmit} className = 'formSearch'>
            <input type ='text' value = {search} onChange = {onSeachChange} placeholder = 'search Github Users' className='searchPlaceholder'/>
            <Button  type = 'submit' onClick ={handleSubmit}  className =  'btnSubmit'>SEARCH</Button>
        </form>
    </div>}
        </div>
    )
}

export default SearchBlog;