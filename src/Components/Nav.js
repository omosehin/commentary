import React from 'react'
import Logo from '../asset/logo.jpg';
import { NavLink } from 'react-router-dom'

const Nav = (prop) => {
  return (
    <div>
      <nav className="nav">
        <div className="logo-box">
    <NavLink to='/'>
          <img src={Logo} alt = 'logo' className = 'logo'/>

    </NavLink>
        </div>
        <div className="headerNav">
          <ul className="subNav">
            <li>
              <NavLink to='/login'  activeStyle={{
background:'green',
color:'white'
}}>
                 Sign In
              </NavLink>
              </li>
              <li>
              <NavLink to='/Sign_Up'
               
               activeStyle={{
                background:'green',
                color:'white'
                }}
              >
                 Sign Up
              </NavLink>
              </li>
              <li>
              <NavLink to='/Blog' 
               activeStyle={{
                background:'green',
                color:'white'
                }}
              >
                 Blog
              </NavLink>
              </li>

          </ul>
        </div>
      </nav>
    </div>
  )
}
export default Nav;
