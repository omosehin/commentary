import React,{useState} from 'react'
import '../../App.css'
import { useHistory } from "react-router-dom";
import Button from '../Button'
import Nav from '../Nav'

const initialState = {
    email:'',
    password:'',
    firstName:'',
}
const SignInError = {
    emailError:'',
    passwordError:''
}
const SignUp = () => {
    const [loginuser, setLoginuser] = useState(initialState)
    const [signerror, setSignerror] = useState(SignInError)
    const [isDisabled,setisDisabled] = useState(true);

    let history = useHistory();

    const handleChange =(e)=>{
        const {name,value} = e.target;
        let valid = isValid();
        valid ? setisDisabled(false) : setisDisabled(true);
        setLoginuser({...loginuser,[name]:value})   
    }
    const handleSubmit = (e)=>{
        e.preventDefault()
        setLoginuser(initialState);
        console.log(loginuser);
    }

    const isValid = () => {
        const emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        const { email,password } = loginuser;
        let response = false
          if(!emailRegex.test(email)){
            signerror.emailError = "Valid email address is required!"
            response =  false;
          }
          if(emailRegex.test(email)){
              signerror.emailError = ""
              response =  true;
          }
          if(password.length < 6 ){
            signerror.passwordError = "Password length  must be longer than 5!"
            response =  false;

          }
          if(password.length > 6 ){
              signerror.passwordError = ""
              response =  true;
          }
        return  response

    }

    const gotoSignIn =() =>{
        history.push('/login')
    }
    const {emailError,passwordError} = signerror;
    const {email,password,firstName,number} = loginuser
    return (
        <div>
            <Nav/>
        <div className = 'loginPage'>
            <div className='loginContainer'>
            
            <div>
                <h2 className ="login">SIGN UP</h2>
                <p className ="card-text loginWelcomeMsg">Please fill in your profile</p>
                <form className = 'loginForm' onSubmit = {handleSubmit}>
               
                    <div>
                        <label className = "card-text">First Name</label>
                    <input type = 'text' name = 'firstname' value = {firstName}  onChange = {handleChange}/>
                    </div>
                    <div>
                        <label className = "card-text">Email</label>
                    <input type = 'email' name = 'email' value = {email}  onChange = {handleChange}/>
                    </div>
                    <p className = 'validationErrorNotification'>{emailError}</p>

                    <div>
                        <label className = "card-text">Password</label>
                    <input type = 'password' name = 'password' value = {password} onChange = {handleChange}/>
                    </div>
                    <p className = 'validationErrorNotification'>{ passwordError}</p>

                    {/* <p className ="card-text forgotPassword">Forgot Your Password</p> */}
                    <Button type='submit' disabled={isDisabled} className = {!email || !password ?'loginDisabled':'loginbtn'} onClick = {handleSubmit}>
                        SIGN UP
                    </Button>


                </form>
                <h2 onClick = {gotoSignIn} className = 'gotoRegister'>Already has an account,Sign In</h2>
            </div>
            </div>
            
            
        </div>
        </div>
    )
}

export default SignUp;