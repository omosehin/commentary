import React,{useState} from 'react'
import '../../App.css'
import { useHistory } from "react-router-dom";
import Button from '../Button'
import Nav from '../Nav'

const initialState = {
    email:'',
    password:''
}
const SignInError = {
    emailError:'',
    passwordError:''
}
const SignIn = () => {

    const [loginuser, setLoginuser] = useState(initialState);
    const [signerror, setSignerror] = useState(SignInError)
    const [isDisabled,setisDisabled] = useState(true);
    let history = useHistory();

    const handleChange =(e)=>{
        const {name,value} = e.target;
        let valid = isValid();
        valid ? setisDisabled(false) : setisDisabled(true);
        setLoginuser({...loginuser,[name]:value})   
    }
    

    const isValid = () => {
        const emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        const { email,password } = loginuser;
        let response = false
          if(!emailRegex.test(email)){
            signerror.emailError = "Valid email address is required!"
            response =  false;
          }
          if(emailRegex.test(email)){
              signerror.emailError = ""
              response =  true;
          }
          if(password.length < 6 ){
            signerror.passwordError = "Password length  must be longer than 5!"
            response =  false;

          }
          if(password.length > 6 ){
              signerror.passwordError = ""
              response =  true;
          }
        return  response

    }


    const handleSubmit = (e)=>{
        e.preventDefault()
        if(isValid()){
            setLoginuser(initialState);
            console.log(loginuser);

        }
    }
    

    const gotoRegister =() =>{
        history.push('/Sign_Up')
    }
    const {email,password} = loginuser;
    const {emailError,passwordError} = signerror;
    
    return (
        <div>
            <Nav/>
        <div className = 'loginPage'>
            <div className='loginContainer'>
            
            <div>
                <h2 className ="login">LOGIN</h2>
                <p className ="card-text loginWelcomeMsg">Welcome! Please fill in your email and password</p>
                <form className = 'loginForm' onSubmit = {handleSubmit}>
                    
                    <div>
                        <label className = "card-text">Email</label>
                    <input type = 'text' name = 'email' value = {email}  onChange = {handleChange}/>
                    </div>
                    <p className = 'validationErrorNotification'>{emailError}</p>
                    <div>
                        <label className = "card-text">Password</label>
                    <input type = 'password' name = 'password' value = {password} onChange = {handleChange}/>
                    </div>
                    <p className = 'validationErrorNotification'>{ passwordError}</p>
                    <p className ="card-text forgotPassword">Forgot Your Password</p>
                    <Button type='submit' disabled={isDisabled} className = {!email || !password ?'loginDisabled':'loginbtn'} onClick = {handleSubmit}  >
                        Login Now
                    </Button>


                </form>
                <h2 onClick = {gotoRegister} className = 'gotoRegister'>Dont Have an Account,Sign Up</h2>
            </div>
            </div>
            
            
        </div>
        </div>
    )
}

export default SignIn;