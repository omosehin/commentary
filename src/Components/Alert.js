import React from 'react'
import '../App.css'
const Alert =({myalert})=> {
    return (
        <div className ='alert'>
       { myalert &&
         ( <div className = {`alert alert- ${myalert.type}`}>
            <i className=' myalert'>{myalert.msg}</i>
        </div>)
        }
        </div>
        
    )
}

export default Alert;
