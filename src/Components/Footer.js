import React from 'react'

function Footer(props) {
    return (
        <div className = "footer">

            <div>
                <ul>
                    <li className="foooter_ListHeader">Products</li>
                    <li>PowerBi</li>
                    <li>Data Science Tools</li>
                    <li>Backend Tools</li>                 
                </ul>
            </div>
            <div>
                <ul>
                    <li className="foooter_ListHeader">News</li>
                    <li>Lagos Bridge</li>
                    <li>PowerTech Entries</li>
                    <li>Business Intelligence</li>                 
                </ul>
            </div>
            <div>
                <ul>
                    <li className="foooter_ListHeader">Office</li>
                    <li>Isheri Center</li>
                    <li>Vibranium Valley</li>
                    <li>Ikeja</li>                 
                </ul>
            </div>
            <div>
                <ul>
                    <li className="foooter_ListHeader">Connect</li>
                    <li>Facebook </li>
                    <li>Twitter</li>
                    <li>Linkedin</li>                 
                </ul>
            </div>
        </div>
    )
}
export default Footer

