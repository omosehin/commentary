import React from 'react'

function Button({children,className,disabled}) {
    return (
        <div >
            <button className = {className} disabled = {disabled} >{children}</button>
        </div>
    )
}

export default Button;