import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Homes,Login } from "./Components/Route";
import "./App.css";
import Home from "./Components/Landingpage";
import SignIn from "./Components/Auth/SignIn";
import SignUp from "./Components/Auth/SignUp";
import Blog from "./Components/Blog";
import User from "./Components/User";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path={Homes} component={Home} />
        <Route path={Login} component={SignIn} />
        <Route path="/Sign_Up" component={SignUp} />
        <Route path="/Blog" component={Blog} />
        <Route path="/User/:login" component={User} />
      </Switch>
    </Router>
  );
};

export default App;
